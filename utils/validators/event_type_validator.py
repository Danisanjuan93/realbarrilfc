import re

import utils.errors.errors_code as errors_code
from utils.auth.auth_utils import USER_ACCESS_ROLES
import utils.validators.general_validator as validator

def get_event_by_id_fields_validator(event_type_id):
    if not validator.validate_integer(event_type_id):
        raise ValueError("Field id is required to get an Event Type by id and should be a valid integer", errors_code.EVENT_TYPE_ID_MANDATORY)

def new_event_type_fields_validator(event_type_json):
    if not validator.validate_string(event_type_json.get('name')):
        raise ValueError("Field name is required to create a new Event Type and must be a valid string", errors_code.EVENT_TYPE_NAME_MANDATORY)

    if not validator.validate_string(event_type_json.get('icon'), nullable=True):
        raise ValueError("Field icon must be a valid string", errors_code.VALIDATION_INVALID_STRING)

def update_event_type_fields_validator(event_type_json):
    if not event_type_json.get('id'):
        raise ValueError("Field id is required to update an Event Type", errors_code.EVENT_TYPE_ID_MANDATORY)

    if not validator.validate_string(event_type_json.get('name'), nullable=True):
        raise ValueError("Field name must be a valid string", errors_code.VALIDATION_INVALID_STRING)

    if not validator.validate_string(event_type_json.get('icon'), nullable=True):
        raise ValueError("Field icon must be a valid string", errors_code.VALIDATION_INVALID_STRING)

def delete_event_type_fields_validator(event_type_id):
    if not validator.validate_integer(event_type_id):
        raise ValueError("Field id is required to delete an Event Type and should be a valid integer")
