import os
import base64
from datetime import datetime

from google.cloud import storage


def upload_image(user, bs64_image):
    storage_client = storage.Client()
    path_to_storage = "photos/{}-{}".format(user.id, user.email)

    bucket = storage_client.get_bucket(os.environ['BUCKET_NAME'])
    blobs = bucket.list_blobs(prefix="{}/".format(path_to_storage))
    for blob in blobs:
        blob.delete()

    photo_extension = bs64_image.split('/')[1].split(';')[0]
    content_type = 'image/' + photo_extension

    photo_name = "{}/{}-{}-{}".format(path_to_storage, user.id, user.email, str(datetime.utcnow()))
    photo_data = bs64_image.split(',')[1]

    bucket.blob(photo_name).upload_from_string(base64.b64decode(photo_data), content_type=content_type)

    bucket.blob(photo_name).make_public()
    return bucket.blob(photo_name).public_url

def remove_user_photo(user):
    storage_client = storage.Client()
    path_to_storage = "photos"

    bucket = storage_client.get_bucket(os.environ['BUCKET_NAME'])
    blobs = bucket.list_blobs(prefix="{}/".format(path_to_storage))
    for blob in blobs:
        if blob.name.split('/')[1] == f"{user.id}-{user.email}":
            blob.delete()
            break
