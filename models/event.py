import json
import bcrypt
from datetime import datetime

from sqlalchemy.orm import relationship
from sqlalchemy.ext.declarative import declarative_base

from app import db

import utils.errors.errors_code as errors_code
import utils.validators.user_validator as validator


Base = declarative_base()

class Event(db.Model):
    __tablename__ = 'events'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(), nullable=False)
    start_date = db.Column(db.Date(), nullable=False)
    start_time = db.Column(db.Time())
    creation_date = db.Column(db.DateTime(), default=datetime.now())
    location = db.Column(db.String())
    price = db.Column(db.Float(), default=0.0)
    min_users = db.Column(db.Integer(), nullable=False)
    max_users = db.Column(db.Integer())
    created_by = db.Column(db.Integer(), db.ForeignKey('users.id'), nullable=False)
    description = db.Column(db.Text())
    event_type = db.Column(db.Integer(), db.ForeignKey('events_types.id'), nullable=False)
    duration = db.Column(db.String())

    def __init__(self, id=None, name=None):
        self.id = id
        self.name = name

    def __repr__(self):
        return '<id {}>'.format(self.id)
    
    def serialize(self):
        return {
            'id': self.id, 
            'name': self.name
        }
