import bcrypt
from sqlalchemy.orm import relationship

from app import db

import utils.errors.errors_code as errors_code
import utils.validators.user_validator as validator

class EventType(db.Model):
    __tablename__ = 'events_types'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String())
    icon = db.Column(db.String())

    def __init__(self, id=None, name=None, icon=None):
        self.id = id
        self.name = name
        self.icon = icon

    def __repr__(self):
        return '<id {}>'.format(self.id)
    
    def serialize(self):
        return {
            'id': self.id, 
            'name': self.name,
            'icon': self.icon
        }

    @staticmethod
    def from_json_to_model(event_type_json):
        event_type = EventType(name=event_type_json.get('name'), icon=event_type_json.get('icon'))
        return event_type
