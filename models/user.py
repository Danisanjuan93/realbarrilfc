import json
import bcrypt
from sqlalchemy.orm import relationship
from sqlalchemy.ext.declarative import declarative_base

from app import db

import utils.errors.errors_code as errors_code
import utils.validators.user_validator as validator

user_event_type_relationship = db.Table('user_event_type_relationship', db.Model.metadata,
    db.Column('user_id', db.Integer, db.ForeignKey('users.id')),
    db.Column('event_type_id', db.Integer, db.ForeignKey('events_types.id'))
)

user_event_relationship = db.Table('user_event_relationship', db.Model.metadata,
    db.Column('user_id', db.Integer, db.ForeignKey('users.id')),
    db.Column('event_type_id', db.Integer, db.ForeignKey('events.id'))
)

class User(db.Model):
    __tablename__ = 'users'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String, nullable=False)
    surname = db.Column(db.String)
    nickname = db.Column(db.String, unique=True)
    email = db.Column(db.String, unique=True, nullable=False)
    password = db.Column(db.LargeBinary, nullable=False)
    role = db.Column(db.Integer, default=0, nullable=False)
    birthday = db.Column(db.String)
    image = db.Column(db.String)
    events_types = db.relationship("EventType", secondary=user_event_type_relationship, backref="users", lazy='dynamic')

    events = db.relationship("Event", secondary=user_event_relationship)

    def __init__(self, id=None, name=None, surname=None, nickname=None, email=None, password=None, role=None,
                birthday=None, image=None, events_types=[], events=[]):
        self.id = id
        self.name = name
        self.surname = surname
        self.nickname = nickname
        self.email = email
        self.password = password
        self.birthday = birthday
        self.role = role
        self.image = image
        self.events_types = events_types
        self.events = events

    def __repr__(self):
        return '<id {}>'.format(self.id)
    
    def full_serialize(self):
        return {
            'id': self.id,
            'name': self.name,
            'surname': self.surname,
            'nickname': self.nickname,
            'email': self.email,
            'birthday': self.birthday,
            'role': self.role,
            'image': self.image,
            'events_types': [event_type.serialize() for event_type in self.events_types.all()] if self.events_types else [],
            'events': [event.serialize() for event in self.events.all()] if self.events else []
        }
    
    def partial_serialize(self):
        return {
            'id': self.id,
            'name': self.name,
            'surname': self.surname,
            'nickname': self.nickname,
            'email': self.email,
            'role': self.role,
            'image': self.image,
        }

    def update_user_values(self, new_data):
        if new_data.get('name'):
            self.name = new_data.get('name').strip()
        if new_data.get('surname'):
            self.surname = new_data.get('surname').strip()
        if new_data.get('email'):
            self.email = new_data.get('email').strip()
        if new_data.get('birthday'):
            self.birthday = new_data.get('birthday').strip()
        if new_data.get('password'):
            self.password = bcrypt.hashpw(new_data.get('password').encode('utf-8'), bcrypt.gensalt())
        if new_data.get('nickname'):
            self.nickname = new_data.get('nickname').strip()
        if new_data.get('role') is not None:
            self.role = int(new_data.get('role'))
        if new_data.get('events_types') is not None:
            self.events_types = new_data.get('events_types')

    @staticmethod
    def from_json_to_model(json):
        user = User(name=json.get('name'), surname=json.get('surname'), nickname=json.get('nickname'), email=json.get('email'), password=bcrypt.hashpw(json.get('password').encode('utf-8'), bcrypt.gensalt()), 
                    birthday=json.get('birthday'), role=json.get('role'), image=json.get('image'), events_types=json.get('events_types', []), events=json.get('events', []))
        return user
