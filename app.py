import os
import jwt
import json

from flask import Flask, request, g, jsonify, session
from flask_sqlalchemy import SQLAlchemy
from flask_httpauth import HTTPTokenAuth


app = Flask(__name__)
auth = HTTPTokenAuth(scheme='Token')
app.config.from_object(os.environ['APP_SETTINGS'])
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

db = SQLAlchemy(app)

import controllers.user as user_controller
import controllers.event_type as event_type_controller

import utils.auth.auth_utils as auth_utils
from utils.errors.error_manager import return_error_code

@auth.verify_token
def verify_token(token):
    user_id = auth_utils.decode_auth_token(token)
    if not user_id:    
        return False

    response = user_controller.get_user_by_id(user_id)
    
    if response[1] == 200:
        user_decoded = json.loads(response[0].response[0].decode("UTF-8"))
        session['user_id'] = int(user_decoded['id'])
        session['role'] = int(user_decoded['role'])
        return True
    else:
        return False

@auth.error_handler
def auth_error():
    return jsonify({'status': 'error', 'message': 'Invalid token provided. Acces Denied'}), 401


@app.route("/user", methods=["GET"])
@auth.login_required
@auth_utils.requires_access_level([auth_utils.USER_ACCESS_ROLES['admin'], auth_utils.USER_ACCESS_ROLES['user']])
def get_users():
    try:
        users = user_controller.get_all_users()
        return users
    except Exception as exception:
        return jsonify(return_error_code(exception)), 400

@app.route("/user/<id>", methods=["GET"])
@auth.login_required
@auth_utils.requires_access_level([auth_utils.USER_ACCESS_ROLES['admin'], auth_utils.USER_ACCESS_ROLES['user']])
def get_user_by_id(id):
    try:
        user = user_controller.get_user_by_id(id)
        return user
    except Exception as exception:
        return jsonify(return_error_code(exception)), 400

@app.route("/user", methods=["PUT"])
@auth.login_required
@auth_utils.requires_access_level([auth_utils.USER_ACCESS_ROLES['admin'], auth_utils.USER_ACCESS_ROLES['user']])
def update_user():
    try:
        response = user_controller.update_user(request.json)
        return response
    except Exception as exception:
        return jsonify(return_error_code(exception)), 400

@app.route("/user/register", methods=["POST"])
def register_user():
    try:
        response = user_controller.register_new_user(request.json)
        return response
    except Exception as exception:
        return jsonify(return_error_code(exception)), 400

@app.route("/user/<user_id>", methods=["DELETE"])
@auth.login_required
@auth_utils.requires_access_level([auth_utils.USER_ACCESS_ROLES['admin'], auth_utils.USER_ACCESS_ROLES['user']])
def delete_user(user_id):
    try:
        response = user_controller.delete_user(user_id)
        return response
    except Exception as exception:
        return jsonify(return_error_code(exception)), 400

@app.route("/user/status", methods=["PUT"])
@auth.login_required
@auth_utils.requires_access_level([auth_utils.USER_ACCESS_ROLES['admin'], auth_utils.USER_ACCESS_ROLES['user']])
def change_user_status():
    try:
        response = user_controller.change_user_status(request.json)
        return response
    except Exception as exception:
        return jsonify(return_error_code(exception)), 400

@app.route("/user/login", methods=["POST"])
def login_user():
    try:
        response = user_controller.login_user(request.json)
        return response
    except Exception as exception:
        return jsonify(return_error_code(exception)), 400

@app.route("/event_type", methods=["GET"])
@auth.login_required
@auth_utils.requires_access_level([auth_utils.USER_ACCESS_ROLES['admin'], auth_utils.USER_ACCESS_ROLES['user']])
def get_events_types():
    try:
        response = event_type_controller.get_events_types()
        return response
    except Exception as exception:
        return jsonify(return_error_code(exception)), 400

@app.route("/event_type/<event_type_id>", methods=["GET"])
@auth.login_required
@auth_utils.requires_access_level([auth_utils.USER_ACCESS_ROLES['admin'], auth_utils.USER_ACCESS_ROLES['user']])
def get_event_type_by_id(event_type_id):
    try:
        response = event_type_controller.get_event_type_by_id(event_type_id)
        return response
    except Exception as exception:
        return jsonify(return_error_code(exception)), 400

@app.route("/event_type", methods=["POST"])
@auth.login_required
@auth_utils.requires_access_level([auth_utils.USER_ACCESS_ROLES['admin'], auth_utils.USER_ACCESS_ROLES['user']])
def create_event_type():
    try:
        response = event_type_controller.create_event_type(request.json)
        return response
    except Exception as exception:
        return jsonify(return_error_code(exception)), 400

@app.route("/event_type", methods=["PUT"])
@auth.login_required
@auth_utils.requires_access_level([auth_utils.USER_ACCESS_ROLES['admin'], auth_utils.USER_ACCESS_ROLES['user']])
def update_event_type():
    try:
        response = event_type_controller.update_event_type(request.json)
        return response
    except Exception as exception:
        return jsonify(return_error_code(exception)), 400

@app.route("/event_type/<event_type_id>", methods=["DELETE"])
@auth.login_required
@auth_utils.requires_access_level([auth_utils.USER_ACCESS_ROLES['admin'], auth_utils.USER_ACCESS_ROLES['user']])
def delete_event_type(event_type_id):
    try:
        response = event_type_controller.delete_event_type(event_type_id)
        return response
    except Exception as exception:
        return jsonify(return_error_code(exception)), 400

if __name__ == '__main__':
    app.run()