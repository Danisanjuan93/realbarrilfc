from flask import jsonify, session

from app import db

from models.user import User
from models.event_type import EventType

import utils.errors.error_manager as error_manager
import utils.errors.errors_code as errors_code
import utils.validators.event_type_validator as event_type_validator

def get_events_types_by_ids(events_types_id_list):
    result = []
    for event_type_id in events_types_id_list:
        event_type = db.session.query(EventType).filter(EventType.id == event_type_id).first()
        if event_type:
            result.append(event_type)

    return result

def get_events_types():
    events_types = db.session.query(EventType).all()

    result = [event_type.serialize() for event_type in events_types]

    return jsonify(result), 200

def get_event_type_by_id(event_type_id):
    event_type_validator.get_event_by_id_fields_validator(event_type_id)

    event_type = db.session.query(EventType).filter(EventType.id == event_type_id).first()

    if not event_type:
        return error_manager.return_error_code(ValueError("Event Type does not exists", errors_code.EVENT_TYPE_DOES_NOT_EXISTS)), 404

    return jsonify(event_type.serialize()), 200

def create_event_type(event_type_json):
    event_type_validator.new_event_type_fields_validator(event_type_json)

    new_event_type = EventType.from_json_to_model(event_type_json)

    db.session.add(new_event_type)
    db.session.commit()

    return jsonify({"status": "ok", "message": "Event Type created"}), 200

def update_event_type(event_type_json):
    event_type_validator.update_event_type_fields_validator(event_type_json)

    event_type = db.session.query(EventType).filter(EventType.id == event_type_json.get('id')).first()
    
    if not event_type:
        return error_manager.return_error_code(ValueError("Event Type does not exists", errors_code.EVENT_TYPE_DOES_NOT_EXISTS)), 404

    event_type.name = event_type_json.get('name', event_type.name)
    event_type.icon = event_type_json.get('icon', event_type.icon)

    db.session.commit()

    return jsonify({"status": "ok", "message": "Event Type updated"}), 200

def delete_event_type(event_type_id):
    event_type_validator.delete_event_type_fields_validator(event_type_id)

    event_type = db.session.query(EventType).filter(EventType.id == event_type_id).first()
    
    if not event_type:
        return error_manager.return_error_code(ValueError("Event Type does not exists", errors_code.EVENT_TYPE_DOES_NOT_EXISTS)), 404
    
    # user = db.session.query(User).filter(User.id == session['user_id']).first()

    # user.events_types.remove(event_type)

    db.session.delete(event_type)
    db.session.commit()

    return jsonify({"status": "ok", "message": "Event Type deleted"}), 200
